import React, { useEffect, useRef, useState } from 'react'
import './App.css'
import Grid from './components/Grid'
import jsonPatterns from './grids.json'

function App() {
	const [launched, setLaunched] = useState(false)
	const [iteration, setIteration] = useState(0)
	const [tickRate, setTickRate] = useState(200)
	const [patterns, setPatterns] = useState(jsonPatterns)
	const [grid, setGrid] = useState([,])
	const nbRows = 80
	const nbColumns = 150
	const intervalId = useRef()

	const generateGrid = () => {
		setGrid(null)
		let tempGrid = Array(nbRows)
			.fill(0)
			.map(() => Array(nbColumns).fill(0))
		tempGrid.map((array, indexA) => {
			array.map((cell, indexC) => {
				tempGrid[indexA][indexC] = Math.random() > 0.5 ? 1 : 0
			})
		})
		return tempGrid
	}

	const clearGrid = () => {
		setGrid(null)
		let tempGrid = Array(nbRows)
			.fill(0)
			.map(() => Array(nbColumns).fill(0))
		return tempGrid
	}

	useEffect(() => {
		setGrid(generateGrid())
		setPatterns(jsonPatterns)
	}, [])

	useEffect(() => {
		if (launched) {
			intervalId.current = setInterval(() => {
				setIteration(iteration + 1)
			}, tickRate)
			return () => clearInterval(intervalId.current)
		}
	}, [launched, iteration, tickRate])

	const handleClickStart = () => {
		setLaunched(true)
	}

	const handleChangeTickRate = event => {
		setTickRate(parseInt(event.target.value))
	}

	const handleChangePattern = event => {
		event.target.value &&
			setGrid(
				patterns.grids.filter(e => e.name === event.target.value).pop().value,
			)
	}

	const handleClickStop = () => {
		setLaunched(false)
		setIteration(0)
		clearInterval(intervalId.current)
	}

	const handleClickGenerate = () => {
		setGrid(generateGrid())
	}

	const handleClickClear = () => {
		setGrid(clearGrid())
	}

	return (
		<div className='App'>
			<header className='App-header'>
				<Grid launched={launched} iteration={iteration} grid={grid} />
				<div>
					<button className='button' onClick={handleClickStart}>
						START
					</button>
					<button className='button' onClick={handleClickStop}>
						STOP
					</button>
					<button className='button' onClick={handleClickGenerate}>
						REGENERATE
					</button>
					<button className='button' onClick={handleClickClear}>
						CLEAR
					</button>
					<label className='input'>TICK RATE (ms)</label>

					<input
						type='number'
						id='input'
						className='input input-text'
						defaultValue={tickRate}
						onChange={e => handleChangeTickRate(e)}
					/>
					<label className='input' htmlFor='pattern-select'>
						Choose a pattern:
					</label>
					<select
						name='patterns'
						id='pattern-select'
						className='input input-text'
						onChange={e => handleChangePattern(e)}
					>
						<option value=''>--Choose a pattern--</option>
						{patterns.grids.map(element => {
							return (
								<option key={element.value} value={element.name}>
									{element.name}
								</option>
							)
						})}
					</select>
				</div>
			</header>
		</div>
	)
}

export default App
