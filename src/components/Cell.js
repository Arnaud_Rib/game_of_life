import { React, useState } from 'react'
import './../App.css'

export default function Cell({ value }) {
	const [active, setActive] = useState(value)

	// const handleClick = event => {
	// 	setActive(active === 1 ? 0 : 1)
	// }

	return (
		<div
			className={active === 1 ? 'blackCell' : 'cell'}
			// onClick={handleClick}
		></div>
	)
}
