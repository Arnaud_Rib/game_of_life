import React, { useCallback, useEffect, useState } from 'react'

export default function Grid({ launched, iteration, grid }) {
	const [UIgrid, setUIgrid] = useState(grid)
	const [UINextgrid, setUINextgrid] = useState(grid)

	useEffect(() => {
		setUIgrid(grid)
		setUINextgrid(grid)
	}, [grid])

	// Count neighbours for GOL rules
	const countNeighbours = useCallback((row, col) => {
		let total_neighbours = 0
		total_neighbours += setCellValueHelper(row - 1, col - 1)
		total_neighbours += setCellValueHelper(row - 1, col)
		total_neighbours += setCellValueHelper(row - 1, col + 1)
		total_neighbours += setCellValueHelper(row, col - 1)
		total_neighbours += setCellValueHelper(row, col + 1)
		total_neighbours += setCellValueHelper(row + 1, col - 1)
		total_neighbours += setCellValueHelper(row + 1, col)
		total_neighbours += setCellValueHelper(row + 1, col + 1)
		return total_neighbours
	})

	// helper for rules
	const setCellValueHelper = (row, col) => {
		try {
			return UIgrid[row][col]
		} catch {
			return 0
		}
	}

	// update cell value from rules
	const updateCell = (row, col) => {
		const total = countNeighbours(row, col)
		if (UIgrid[row][col] === 1) {
			if (total < 2 || total > 3) {
				return 0
			} else return 1
		} else if (UIgrid[row][col] === 0) {
			if (total === 3) return 1
			else return 0
		}
	}

	useEffect(() => {
		var nextgrid = null
		if (launched) {
			nextgrid = new Array(UIgrid.length)
				.fill(0)
				.map(() => new Array(UIgrid[0].length).fill(0))

			UIgrid.map((array, indexA) => {
				array.map((cell, indexC) => {
					nextgrid[indexA][indexC] = updateCell(indexA, indexC)
				})
			})
			setUIgrid(nextgrid)
		}
	}, [iteration, launched])

	const handleClick = (cell, row, col) => {
		UIgrid[row][col] = cell === 1 ? 0 : 1
		setUIgrid([...UIgrid])
	}

	return (
		<div>
			{UIgrid.map((array, indexA) => {
				return (
					<div className='grid' key={indexA}>
						{array.map((cell, indexC) => {
							return (
								<div
									key={indexA * 2 + indexC}
									className={cell === 1 ? 'blackCell' : 'cell'}
									onClick={() => handleClick(cell, indexA, indexC)}
								></div>
							)
						})}
					</div>
				)
			})}
		</div>
	)
}
